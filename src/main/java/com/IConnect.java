package com;

public interface IConnect {

    public void MakeConnection();
    public void CloseConnection();
}
